﻿using AutoMapper;
using NHibernate.Mapping.ByCode.Impl;
using Questao2.Mapping;
using Questao2.Models;
using Questions.Data.BaseModel.Context;
using Questions.Data.BaseModel.Models;
using Questions2.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace Questao2.Controllers
{
    public class SalesOrderController : Controller
    {
        //
        // GET: /SalesOrder/
        private ISalesOrderService salesOrderService;

        private IMapper imapper;
        
        public SalesOrderController(ISalesOrderService salesOrderService)
        {
            this.salesOrderService = salesOrderService;

            var config = new AutoMapperConfig().Configure();
            imapper = config.CreateMapper();

        }
        public ActionResult Index()
        {

            // TODO: Retornar a coleção de pedidos de vendas (SalesOrder).
         
            var listSalesOrder = new List<Foo>();
           
            foreach (var sale in salesOrderService.GetSalesOrders())
            {
                listSalesOrder.Add(new Foo
                {
                    Title = sale.Id.ToString(),
                    Description = sale.Description
                });
            }
            return View(listSalesOrder);
        }

        //
        // GET: /SalesOrder/Create

        public ActionResult Create()
        {
            // TODO: Implementação.
            var listUsers = salesOrderService.GetUsers();
            
            ViewBag.UsersList = salesOrderService.carregarListSalesOrder();

            return View();
        } 

        //
        // POST: /SalesOrder/Create

        [HttpPost]
        public ActionResult Create(SalesOrderView salesOrder)
        {

            // TODO: Implementação.
            try
            {          
               
                var salesOrderSave = imapper.Map<SalesOrder>(salesOrder);

                salesOrderService.Save(salesOrderSave);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /SalesOrder/Edit/5
 
        public ActionResult Edit(int id)
        {
            // TODO: Implementação.
            var sale = salesOrderService.Get(id);
            ViewBag.UsersList = salesOrderService.carregarListSalesOrder();

            var salesView = imapper.Map<SalesOrderView>(sale);

            return View(salesView);
        }

        //
        // POST: /SalesOrder/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, SalesOrder salesOrder)
        {
            // TODO: Implementação.
            try
            {
                // TODO: Add update logic here
                var saleAlterado = salesOrderService.Get(id);

                saleAlterado.Title = salesOrder.Title;
                saleAlterado.Description = salesOrder.Description;
                saleAlterado.Value = salesOrder.Value;
                saleAlterado.UserId = salesOrder.UserId;

                salesOrderService.Save(saleAlterado);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        //
        // POST: /SalesOrder/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            // TODO: Implementação.
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
