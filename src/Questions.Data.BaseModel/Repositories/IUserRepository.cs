﻿using Questions.Data.BaseModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questions.Data.BaseModel.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
    }
}
