﻿using NHibernate;
using Questions.Data.BaseModel.Context;
using Questions.Data.BaseModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questions.Data.BaseModel.Repositories
{
    
    public class UserRepository : IUserRepository
    {
        public IEnumerable<User> GetAll()
        {
            using (ISession session = ContextDataBase.OpenSession())
            {
                var userList = session.Query<User>().ToList();

                session.Close();

                return userList;
            }
        }
    }
}
