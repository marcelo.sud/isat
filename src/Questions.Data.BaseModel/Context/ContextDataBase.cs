﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using Questions.Data.BaseModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;

namespace Questions.Data.BaseModel.Context
{
    public class ContextDataBase
    {
        private static ISessionFactory sessionFactory;
        public static ISession OpenSession()
        {            

            sessionFactory = Fluently.Configure()
                .Database(MsSqlCeConfiguration.Standard
                  .ConnectionString(@"Data Source=C:\TesteMarcelo\questionsTest\src\Questions.Data.BaseModel\App_Data\Database.sdf")
                              //.ShowSql()
                )
               .Mappings(m =>
                          m.FluentMappings
                              .AddFromAssemblyOf<SalesOrder>()
                              .AddFromAssemblyOf<User>())
                .ExposeConfiguration(cfg => new SchemaUpdate(cfg)
                                                .Execute(false, false))
                .BuildSessionFactory();
            return sessionFactory.OpenSession();
        }
        public static void CloseSession()
        {
            sessionFactory.Close();
        }
    }

}
