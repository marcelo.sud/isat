﻿using Questions.Data.BaseModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Services
{
    public interface ISalesOrderService
    {
        IEnumerable<SalesOrder> GetSalesOrders();

        IEnumerable<User> GetUsers();

        SalesOrder Get(int id);

        void Save(SalesOrder salesOrder);

        bool Update(SalesOrder salesOrder);

        List<SelectListItem> carregarListSalesOrder();
    }
}
