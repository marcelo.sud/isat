﻿using FluentNHibernate.Mapping;
using Questions.Data.BaseModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questions.Data.BaseModel.Mapping
{
    class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.CPF);
            
            Table("Users");
        }
    }
}
