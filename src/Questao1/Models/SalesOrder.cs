﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questions1.Models
{
    public class SalesOrder
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual double Value { get; set; }
        public virtual string UserName { get; set; }
        public virtual int UserId { get; set; }
        
    }
}
