using NHibernate;
using Questions.Data.BaseModel.Context;
using Questions.Data.BaseModel.Models;
using System;
using System.Linq;


/*
 *  Questão 1
 *  Faça um programa que liste na console todos os pedidos de venda (SalesOrder) de todos os usuários (User). 
 * 
 * Output esperado
 * User 1
 * -- SalesOrderNumber, TotalValue, ItensCount
 * -- SalesOrderNumber, TotalValue, ItensCount
 * -- SalesOrderNumber, TotalValue, ItensCount
 * User 2
 * -- SalesOrderNumber, TotalValue, ItensCount
 * User N
 * -- SalesOrderNumber, TotalValue, ItensCount
 * -- SalesOrderNumber, TotalValue, ItensCount
 * 
 * Onde "User N" pode ser substituído pelo campo Name da tabela User e SalesOrderNumber, TotalValue, ItensCount os campos da tabela SalesOrder.
 *
 */
namespace Questao1
{
    class Program
    {
        static void Main(string[] args)
        {

            using (ISession session = ContextDataBase.OpenSession())
            {

                var users = session.Query<User>().ToList();
                var sales = session.Query<SalesOrder>().ToList();

                Console.WriteLine("Questão 1 - Listagem de pedidos de vendas de todos os usuários: \r\n");

                foreach (var user in users)
                {
                    Console.WriteLine($"Usuário:  {user.Id}  - Nome:  {user.Name}");

                    var i = 0;
                    foreach (var sale in sales.Where(x => x.UserId.Equals(user.Id)))
                    {
                        i++;
                        Console.WriteLine($"{sale.Title}  -  {sale.Value} - Item: {i}");

                    }
                    var somaValores = (from x in sales where x.UserId.Equals(user.Id) select x.Value).Sum();
                    Console.WriteLine($"Soma das vendas: {somaValores} \r\n");
                }

            }
            Console.ReadKey();

        }
    }
}
