﻿using Questions.Data.BaseModel.Models;
using Questions.Data.BaseModel.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Services
{
    public class SalesOrderService: ISalesOrderService
    {
        private ISalesOrderRepository _iSalesOrderRepository;
        private IUserRepository _iUserRepository;

        public SalesOrderService(ISalesOrderRepository iSalesOrderRepository, IUserRepository iUserRepository)
        {
            _iSalesOrderRepository = iSalesOrderRepository;
            _iUserRepository = iUserRepository;
        }
        public IEnumerable<SalesOrder> GetSalesOrders()
        {
            return _iSalesOrderRepository.GetAll();
        }
        public IEnumerable<User> GetUsers()
        {
            return _iUserRepository.GetAll();
        }

        public void Save(SalesOrder salesOrder)
        {
            _iSalesOrderRepository.Add(salesOrder);
        }

        public SalesOrder Get(int id)
        {
            return _iSalesOrderRepository.Get(id);
        }

        public bool Update(SalesOrder salesOrder)
        {
            return _iSalesOrderRepository.Update(salesOrder);
        }

        public List<SelectListItem> carregarListSalesOrder()
        {
            var listUsers = this.GetUsers();

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in listUsers)
            {
                items.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
            }

            return items;
        }



    }
}
