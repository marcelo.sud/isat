﻿using NHibernate;
using Questions.Data.BaseModel.Context;
using Questions.Data.BaseModel.Models;
using Questions.Data.BaseModel.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questions.Data.BaseModel
{
    public class SalesOrderRespository : ISalesOrderRepository
    {
        ISession session = ContextDataBase.OpenSession();
        public SalesOrder Add(SalesOrder salesOrder)
        {
            if (salesOrder == null)
            {
                throw new ArgumentNullException(nameof(salesOrder));
            }
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Save(salesOrder);
                transaction.Commit();
                session.Close();
            }
            return salesOrder;
        }
        public bool Delete(int id)
        {
            return true;
        }
        public SalesOrder Get(int id)
        {
            var sale = session.Get<SalesOrder>(id);

            session.Close();

            return sale;
        }
        public IEnumerable<SalesOrder> GetAll()
        {
            var sales = session.Query<SalesOrder>().ToList();

            session.Close();

            return sales;
        }
        public bool Update(SalesOrder salesOrder)
        {
            if (salesOrder == null)
            {
                throw new ArgumentNullException(nameof(salesOrder));
            }

            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Save(salesOrder);
                transaction.Commit();
                session.Close();
            }
            return true;
        }
    }
}
