﻿using FluentNHibernate.Mapping;
using Questions.Data.BaseModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Questions.Data.BaseModel
{
    class SalesOrderMap : ClassMap<SalesOrder>
    {
        public SalesOrderMap()
        {
            Id(x => x.Id);
            Map(x => x.Title);
            Map(x => x.Description);
            Map(x => x.Value);
            Map(x => x.UserId);
            //References(x => x.UserId).Column("Id").Cascade.All();

            Table("SalesOrder");
        }
    }
}
