﻿using Questions.Data.BaseModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questions.Data.BaseModel.Repositories
{
    public interface ISalesOrderRepository
    {
        IEnumerable<SalesOrder> GetAll();
        SalesOrder Get(int id);
        SalesOrder Add(SalesOrder item);
        bool Update(SalesOrder item);
        bool Delete(int id);
    }
}
