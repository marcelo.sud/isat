﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Questions.Data.BaseModel.Models;

namespace Questions2.Models
{
    public class SalesOrderView
    {
        public  int Id { get; set; }
        public  string Title { get; set; }
        public  string Description { get; set; }
        public  double Value { get; set; }        
        public  int UserId { get; set; }
                
    }
}
