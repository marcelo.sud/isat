﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Questions.Data.BaseModel.Models;
using Questions2.Models;

namespace Questao2.Mapping
{
    public class AutoMapperConfig
    {
       
        public MapperConfiguration Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {                
                cfg.CreateMap<SalesOrderView, SalesOrder>();             
                cfg.AddProfile<AuthorMappingProfile>();
            }

           );
           return config;
        }
    }

    public class AuthorMappingProfile : Profile
    {
        public AuthorMappingProfile()
        {
            CreateMap<SalesOrderView, SalesOrder>().ReverseMap();
        }
    }
}